# Contribution

Welcome to contribution page.

# Getting started

We provide [devcontainer](.devcontainer) with environment to work with. Once you build and run docker container you have everything you need for contribution. We use [VS code](https://code.visualstudio.com) which leverages dev containers.

## Dev container usage
To run dev container with code you need to do following:
1. Clone repo with code
```
git clone git@gitlab.com:mlops2874914/project-42.git
```
2. Open repo in VS Code
3. Install dev container [extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers) for VS code.
4. Go to command pallet and open project in dev container:
```
View -> Command Pallet -> Dev Containers: Reopen in Container
```
5. Now you can work with code via VS code inside container. You might need a wait a little till extentions are loaded.


You can find more info about dev containers here: [Developing inside a Container](https://code.visualstudio.com/docs/devcontainers/containers).

## Linters and formatters

All settings for linters and formatters are saved in [pyproject](pyproject.toml). Corresponding extentions are defined in [devcontainer.json](.devcontainer/devcontainer.json#L23). Pre-commits is installed during container creation, so you dont need to do anything.

Following commands will helo you to maintain code quality:
* pdm format - format files with ruff
* pdm lint - run ruff linting
* pdm types - run types checking
* pdm check - run everything at once

## Known issue

1. Once dev container is opened, `format on save` [does not work](https://github.com/microsoft/vscode/issues/189839). One needs explicitly reload the window:
```
Command Pallet -> Reload Window
```
