ARG PYTHON_BASE=3.11-slim
# build stage
FROM python:$PYTHON_BASE as builder
RUN apt update \
    && apt install -y --no-install-recommends gcc graphviz python3-dev \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# install PDM
RUN pip install -U pdm
# disable update check
ENV PDM_CHECK_UPDATE=false
# copy files
COPY pyproject.toml pdm.lock README.md /project-prod/
COPY src/ /project-prod/src
RUN cp -r /project-prod /project-dev
# install dependencies and project into the local packages directory
WORKDIR /project-prod
RUN pdm install --check --prod --no-editable

WORKDIR /project-dev
RUN pdm install --check --no-editable

# run stage
FROM python:$PYTHON_BASE as prod-image

# retrieve packages from build stage
COPY --from=builder /project-prod/.venv/ /project/.venv
ENV PATH="/project/.venv/bin:$PATH"

COPY src /project/src
CMD ["python", "project/src/project_42/app.py"]

ARG PYTHON_BASE=3.11-slim
# build stage
FROM python:$PYTHON_BASE as dev-image
RUN apt update \
    && apt install -y --no-install-recommends gcc graphviz python3-dev \
    && apt autoremove -y \
    && rm -rf /var/lib/apt/lists/*

# retrieve packages from build stage
COPY --from=builder /project-dev/.venv/ /project-dev/.venv

ENV PATH="/project-dev/.venv/bin:$PATH"
COPY src /project-dev/src
