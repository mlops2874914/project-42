# project-42

This is inital project for ODS mlops course.

## Usage

Clone project to local system, open it in dev container and you're all set: during container creatation pdm env is activated and pre-commits are installed.

For details take a look [CONTRIBUTING](CONTRIBUTING.md).

## Snakemake routine

We provide example of snakemake routine. Before using it, make sure you have kaggle credentials in `~/.kaggle`. Open project in dev container and run:
```
snakemake --cores 'all'
```

Snakemake pipeline is composed of following rules:

1. [rule all](workflow/Snakefile#L6) - target rule with expected pipeline output.
2. [rule download](workflow/Snakefile#L11) - downloads dataset with data from kaggle
3. [rule unpack](workflow/Snakefile#L18) - unpacks data to working dir
4. [rule prepare_dataset](workflow/Snakefile#L27) - prepares dataset for model training
5. [rule compress_dataset](workflow/Snakefile#L38) - applies PCA to dataset and produces compressed dataset for training
6. [rule model_training](workflow/Snakefile#L49) - trains model on the datasets with different pre-processing. Produces metrics file and trained models.

Graph with snakemake pipeline

![dag](documentation/snakemake_dag.png)


## Docker build

To build docker with project, run:
```
docker build -f Dockerfile -t project-42 . --target prod-image
```
To run docker with proect, run:
```
docker run --rm project-42
```

`prod-image` will contain only production needed packages. At the same time, dev container contains all packages.

## MlFlow

To deploy mlflow locally, you can run:
```
docker compose -f services/mflow.yaml up mlflow
```


## Merge requests
We stick to following routinte:
1. Create new branch with issue key. For instance:
```
git checkout -b 1-set-up-repo
```
2. Do whatever you need.
3. Push branch to repo, create MR and assign it to anyone from the team for code review.
