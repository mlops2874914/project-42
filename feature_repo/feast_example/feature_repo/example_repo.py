# This is an example feature definition file

from datetime import timedelta

import pandas as pd
from feast import (
    Entity,
    FeatureService,
    FeatureView,
    Field,
    FileSource,
    PushSource,
    RequestSource,
)
from feast.on_demand_feature_view import on_demand_feature_view
from feast.types import Float32, Int32, UnixTimestamp

# Read data from parquet files. Parquet is convenient for local development mode. For
# production, you can use your favorite DWH, such as BigQuery. See Feast documentation
# for more info.
energy_consump_source = FileSource(
    name="energy_consump_stats_source",
    path="/workspaces/project-42/feature_repo/feast_example/feature_repo/data/AEP_hourly.parquet",
)

month = Entity(name="month", join_keys=["month"])
# Our parquet files contain sample data that includes a driver_id column, timestamps and
# three feature column. Here we define a Feature View that will allow us to serve this
# data to our model online.
enery_consump_stats = FeatureView(
    # The unique name of this feature view. Two feature views in a single
    # project cannot have the same name
    name="energy_consump_hourly_stats",
    ttl=timedelta(days=0),
    entities=[month],
    # The list of features defined below act as a schema to both define features
    # for both materialization of features into a store, and are used as references
    # during retrieval for building a training dataset or serving features
    schema=[
        Field(name="AEP_MW", dtype=Float32),
        Field(name="event_timestamp", dtype=UnixTimestamp),
        Field(name="dayofyear", dtype=Int32),
        Field(name="year", dtype=Int32),
    ],
    online=True,
    source=energy_consump_source,
    # Tags are user defined key/value pairs that are attached to each
    # feature view
    tags={"team": "energy_stats"},
)

input_request = RequestSource(
    name="timestep",
    schema=[
        Field(name="current_timestep", dtype=UnixTimestamp),
    ],
)


# Define an on demand feature view which can generate new features based on
# existing feature views and RequestSource features
@on_demand_feature_view(
    sources=[enery_consump_stats, input_request],
    schema=[
        Field(name="hour", dtype=Int32),
        Field(name="dayofweek", dtype=Int32),
        Field(name="weekofyear", dtype=Int32),
    ],
)
def transformed_date(inputs: pd.DataFrame) -> pd.DataFrame:
    df = pd.DataFrame()
    df["hour"] = inputs["current_timestep"].dt.hour
    df["dayofweek"] = inputs["current_timestep"].dt.dayofweek
    df["weekofyear"] = inputs["current_timestep"].dt.isocalendar().week
    return df


# This groups features into a model version
daily_consumption_v1 = FeatureService(
    name="daily_consumption_v1",
    features=[
        enery_consump_stats[["AEP_MW"]],  # Sub-selects a feature from a feature view
        transformed_date,  # Selects all features from the feature view
    ],
)

daily_consumption_v2 = FeatureService(
    name="daily_consumption_v2", features=[enery_consump_stats, transformed_date]
)

# Defines a way to push data (to be available offline, online or both) into Feast.
energy_consump_push_source = PushSource(
    name="energy_consump_push_source",
    batch_source=energy_consump_source,
)

# Defines a slightly modified version of the feature view from above, where the source
# has been changed to the push source. This allows fresh features to be directly pushed
# to the online store for this feature view.
energy_consump_fresh_fv = FeatureView(
    name="energy_consump_stats_fresh",
    ttl=timedelta(weeks=100),
    entities=[month],
    schema=[
        Field(name="AEP_MW", dtype=Float32),
        Field(name="event_timestamp", dtype=UnixTimestamp),
        Field(name="dayofyear", dtype=Int32),
        Field(name="year", dtype=Int32),
    ],
    online=True,
    source=energy_consump_push_source,  # Changed from above
    tags={"team": "energy_stats"},
)


# Define an on demand feature view which can generate new features based on
# existing feature views and RequestSource features
@on_demand_feature_view(
    sources=[energy_consump_fresh_fv, input_request],  # relies on fresh version of FV
    schema=[
        Field(name="hour", dtype=Int32),
        Field(name="dayofweek", dtype=Int32),
        Field(name="weekofyear", dtype=Int32),
    ],
)
def transformed_date_fresh(inputs: pd.DataFrame) -> pd.DataFrame:
    df = pd.DataFrame()
    df["hour"] = inputs["current_timestep"].dt.hour
    df["dayofweek"] = inputs["current_timestep"].dt.dayofweek
    df["weekofyear"] = inputs["current_timestep"].dt.isocalendar().week
    return df


energy_consump_v3 = FeatureService(
    name="energy_consump_v3",
    features=[energy_consump_fresh_fv, transformed_date_fresh],
)
