from datetime import datetime

import pandas as pd
from feast import FeatureStore


def fetch_training_dataset(store: FeatureStore, for_batch_scoring: bool):
    entity_df = pd.DataFrame.from_dict(
        {
            # entity's join key -> entity values
            "month": [month_id for month_id in range(1, 13)] * 21,
            "event_timestamp": pd.date_range(
                start="01/01/2014 01:00:00", end="11/01/2018 00:00:00", freq="W"
            ),
            "current_timestep": pd.date_range(
                start="01/01/2014 01:00:00", end="11/01/2018 00:00:00", freq="W"
            ),
        }
    )

    if for_batch_scoring:
        entity_df["event_timestamp"] = pd.to_datetime("now", utc=True)

    training_df = store.get_historical_features(
        entity_df=entity_df,
        features=[
            "energy_consump_hourly_stats:event_timestamp",
            "energy_consump_hourly_stats:AEP_MW",
            "energy_consump_hourly_stats:year",
            "transformed_date:hour",
            "transformed_date:dayofweek",
        ],
    ).to_df()
    return training_df


def fetch_online_features(store, source: str = ""):
    entity_rows = [
        # {join_key: entity_value}
        {"month": 12, "current_timestep": datetime(2024, 12, 11)},
        {"month": 11, "current_timestep": datetime(2024, 11, 11)},
    ]
    if source == "feature_service":
        features_to_fetch = store.get_feature_service("daily_consumption_v1")
    elif source == "push":
        features_to_fetch = store.get_feature_service("energy_consump_v3")
    else:
        features_to_fetch = [
            "energy_consump_hourly_stats:event_timestamp",
            "energy_consump_hourly_stats:AEP_MW",
            "energy_consump_hourly_stats:year",
            "transformed_date:hour",
            "transformed_date:dayofweek",
        ]
    returned_features = store.get_online_features(
        features=features_to_fetch,
        entity_rows=entity_rows,
    ).to_dict()
    for key, value in sorted(returned_features.items()):
        print(key, " : ", value)


if __name__ == "__main__":
    store = FeatureStore(repo_path=".")
    training_df = fetch_training_dataset(store, False)

    store.materialize_incremental(end_date=datetime.now())
    print("\n--- Online features ---")
    print("feature service")
    fetch_online_features(store, source="feature_service")
    print("push")
    fetch_online_features(store, source="push")
    # fetch_online_features(store)
    # fetch_online_features(store, source="feature_service")
    # training_df = fetch_training_dataset(store)

    # print(training_df)
