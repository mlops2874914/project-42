### lakefs example

To run set up lakes from, run docker compose from project root:
```
docker compose -f lakefs/docker-compose.yaml up
```

To run snakemake execute from project root:
```
Snakemake -s workflow/Snakefile.lakefs --cores 'all'
```
