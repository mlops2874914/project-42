import cv2
import numpy as np


def get_package_version(package_name: str) -> None:
    """Prints package version

    Args:
        package_name (str): name of package
    """
    if package_name == "cv2":
        print(cv2.__version__)
    elif package_name == "numy":
        print(np.__version__)
    else:
        print(f"Package {package_name} is not available")
