import json
from pathlib import Path

import dvc  # type: ignore
import dvc.api  # type: ignore
import hydra
import joblib  # type: ignore
import numpy as np
import pandas as pd
import scipy  # type: ignore
import scipy.sparse  # type: ignore
from matplotlib import pyplot as plt
from matplotlib.figure import Figure
from sklearn.ensemble import RandomForestClassifier  # type: ignore
from sklearn.linear_model import LogisticRegression  # type: ignore
from sklearn.metrics import (  # type: ignore
    ConfusionMatrixDisplay,  # type: ignore
    classification_report,  # type: ignore
)
from sklearn.model_selection import GridSearchCV, train_test_split  # type: ignore
from sklearn.svm import SVC  # type: ignore


def conf_matrix(y_true: np.ndarray, pred: np.ndarray) -> Figure:
    plt.ioff()
    fig, ax = plt.subplots(figsize=(5, 5))
    ConfusionMatrixDisplay.from_predictions(y_true, pred, ax=ax, colorbar=False)
    ax.xaxis.set_tick_params(rotation=90)
    _ = ax.set_title("Confusion Matrix")
    plt.tight_layout()
    return fig


def train_knn(src_path: str, dst_path: str, cfg: dict, threads: int) -> None:
    """Trains KNN classifier on provided data and
    saves model and metrics

    Args:
        src_path (str): input csv or npy dataset
        dst_path (str): path to save model and metrics
        threads (int): n_jobs knn train
    """
    if Path(src_path).suffix == ".csv":
        train_dataframe = pd.read_csv(src_path)

        y = train_dataframe["quality"]
        x = train_dataframe.drop("quality", axis=1)
    elif Path(src_path).suffix == ".npy":
        data = np.load(src_path)
        y = data[:, 0]
        x = data[:, 1:]

    knn = hydra.utils.instantiate(cfg["classifier"])

    x_train, x_test, y_train, y_test = train_test_split(
        x, y, test_size=0.25, shuffle=True, random_state=1
    )

    knn_cv_model = GridSearchCV(knn, dict(cfg["knn_params"]), cv=5, n_jobs=threads)
    knn_cv_model.fit(x_train, y_train)

    knn_model = hydra.utils.instantiate(
        cfg["classifier"],
        n_neighbors=knn_cv_model.best_params_["n_neighbors"],
        leaf_size=knn_cv_model.best_params_["leaf_size"],
        weights=knn_cv_model.best_params_["weights"],
        n_jobs=threads,
    )

    knn_model.fit(x_train, y_train)

    y_pred = knn_model.predict(x_test)
    report = classification_report(y_test, y_pred, output_dict=True)
    report_dataframe = pd.DataFrame.from_dict(report)
    report_dataframe.to_csv(dst_path)

    metrics_dst_path = Path(dst_path).with_name(f"metrics-{Path(dst_path).stem}.csv")
    report_dataframe.to_csv(metrics_dst_path)

    model_dst_path = Path(dst_path).parent / f"knn-{Path(src_path).suffix[1:]}.npy"

    model_dst_path.parent.mkdir(parents=True, exist_ok=True)
    with open(model_dst_path, "wb") as f:
        joblib.dump(knn_model, f)


def train_logreg(target_data: str, features_data: str, dst_dir: str):
    """Train logistic regression on vectorized features
    To archive reproducability, regression coefficients and bias
    are rounded to 6 decimals

    Args:
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """
    params = dvc.api.params_show()

    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)

    classifier = LogisticRegression(random_state=params["random_state"])
    classifier.fit(features, target)
    classifier.coef_ = np.round(classifier.coef_, 6)
    classifier.intercept_ = np.round(classifier.intercept_, 6)
    joblib.dump(classifier, Path(f"{dst_dir}/logreg.model").open("wb"))


def apply_logreg(model_path: str, target_data: str, features_data: str, dst_dir: str):
    """Apply logistic regression model to dataset
    Args:
        model_path (str): path to logistic regression model
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """
    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)

    classifier = joblib.load(Path(model_path).open("rb"))

    predicts = classifier.predict(features)

    fig = conf_matrix(target, predicts)

    report = classification_report(target, predicts, output_dict=True)

    json.dump(report, Path(f"{dst_dir}/logreg_metrics.json").open("w"), indent=4)
    fig.savefig(Path(f"{dst_dir}/logreg_conf_matrix.png"))


def train_rf(target_data: str, features_data: str, dst_dir: str):
    """Train random forest classifier on vectorized feautures

    Args:
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """
    params = dvc.api.params_show()
    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)
    classifier = RandomForestClassifier(**params["random_forest"])
    classifier.fit(features, target)

    joblib.dump(classifier, Path(f"{dst_dir}/rf.model").open("wb"))


def apply_rf(model_path: str, target_data: str, features_data: str, dst_dir: str):
    """Apply random forest classifier model to dataset

    Args:
        model_path (str): path to random forest classifier
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """
    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)

    classifier = joblib.load(Path(model_path).open("rb"))

    predicts = classifier.predict(features)

    fig = conf_matrix(target, predicts)

    report = classification_report(target, predicts, output_dict=True)

    json.dump(report, Path(f"{dst_dir}/rf_metrics.json").open("w"), indent=4)
    fig.savefig(Path(f"{dst_dir}/rf_conf_matrix.png"))


def train_svm(target_data: str, features_data: str, dst_dir: str):
    """Train svm classifier on vectorized feautures

    Args:
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """

    params = dvc.api.params_show()

    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)
    classifier = SVC(
        random_state=params["random_state"],
        max_iter=1400,
        kernel="rbf",
        C=42,
    )
    classifier.fit(features, target)

    classifier.intercept_ = np.round(classifier.intercept_, 6)

    joblib.dump(classifier, Path(f"{dst_dir}/svm.model").open("wb"))


def apply_svm(model_path: str, target_data: str, features_data: str, dst_dir: str):
    """Apply svm classifier model to dataset

    Args:
        model_path (str): path to random forest classifier
        target_data (str): path to dataset targets file
        features_data (str): path to dataset features file
        dst_dir (str): destination directory
    """
    target = np.load(target_data)
    features = scipy.sparse.load_npz(features_data)

    classifier = joblib.load(Path(model_path).open("rb"))

    predicts = classifier.predict(features)

    fig = conf_matrix(target, predicts)

    report = classification_report(target, predicts, output_dict=True)

    json.dump(report, Path(f"{dst_dir}/svm_metrics.json").open("w"), indent=4)
    fig.savefig(Path(f"{dst_dir}/svm_conf_matrix.png"))
