import re
from typing import Optional

import dvc  # type: ignore
import dvc.api  # type: ignore
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA  # type: ignore
from sklearn.model_selection import train_test_split  # type: ignore
from sklearn.preprocessing import StandardScaler, normalize  # type: ignore


class IllegalValueError(ValueError):
    pass


def create_training_dataframe(src_path: str, dst_path: str, cfg: dict) -> None:
    """Simple dataframe processing with `quality` column
    transformation

    Args:
        src_path (str): input csv file
        dst_path (str): output csv file
        cfg (dict): preprocessor parameters
    """
    wine_dataframe = pd.read_csv(src_path)

    wine_dataframe["quality"] = np.where(
        wine_dataframe["quality"] > cfg["threshold"], 1, 0
    )

    wine_dataframe.to_csv(dst_path)


def compress_data(
    wine_dataframe: str | pd.DataFrame, cfg: dict, dst_path: str | None
) -> Optional[np.ndarray]:
    """Applies PCA to dataset and save dataset as
    numpy file

    Args:
        src_path (str): input csv file
        dst_path (str): output npy file
        cfg (dict): preprocessor parameters
    """
    if isinstance(wine_dataframe, str):
        wine_dataframe = pd.read_csv(wine_dataframe)
    features_df = wine_dataframe.drop("quality", axis=1)
    std_scaler = StandardScaler()
    scaled_features = std_scaler.fit_transform(features_df)

    pca = PCA(n_components=cfg["n_components"])
    compressed_features = pca.fit_transform(scaled_features)

    target = wine_dataframe["quality"]
    array = np.hstack([np.expand_dims(target.values, 1), compressed_features])  # type: ignore
    if dst_path is None:
        return array
    np.save(dst_path, array)
    return None


def split_dataset(src_path: str, dst_dir: str):
    """Split training dataset into train and test parts

    Args:
        src_path (str): path to train csv
        dst_dir (str): dir to save splits
    """
    params = dvc.api.params_show()
    df = pd.read_csv(src_path)

    df.sentiment = df.sentiment.replace({1: 0, 2: 1})
    train, test = train_test_split(
        df, test_size=params["test_size"], random_state=params["random_state"]
    )

    train.to_csv(f"{dst_dir}/train_split.csv")
    test.to_csv(f"{dst_dir}/test_split.csv")


def text_dataset_processing(src_path: str, dst_dir: str, text_column: str = "content"):
    """Apply simple text processing to dataset

    Args:
        src_path (str): path to dataset
        dst_dir (str): destination dir
        text_column (str, optional): column with text. Defaults to "content".
    """
    params = dvc.api.params_show()
    df = pd.read_csv(
        src_path,
        header=None,
        names=["sentiment", "title", "text"],
        nrows=params["nrows"],
    )
    df.fillna("", inplace=True)
    df[text_column] = df["title"] + " " + df["text"]

    df["text_processed"] = (
        df[text_column].str.lower().apply(lambda x: re.sub(r"[^\w\s]|\d|\r|\n", "", x))
    )

    df.to_csv(f"{dst_dir}/processed_dataset.csv")


def get_similar_pairs_naive(embeddings: list[np.ndarray], sim_th: float) -> list[tuple]:
    pairs = []

    unique = set()
    for i in range(0, len(embeddings)):
        current_sim = np.inf
        current_idx = -1
        for j in range(0, len(embeddings)):
            emb_x = embeddings[i]
            emb_y = embeddings[j]

            emb_x = emb_x / np.linalg.norm(emb_x)
            emb_y = emb_y / np.linalg.norm(emb_y)

            similarity = np.dot(emb_x, emb_y)

            if similarity <= sim_th and similarity < current_sim:
                current_sim = similarity
                current_idx = j
        if current_idx != -1 and (current_idx, i) not in unique:
            pairs.append((i, current_idx, current_sim))
            unique.add((i, current_idx))

    return pairs


def get_similar_pairs_opt(embeddings: list[np.ndarray], sim_th: float) -> list[tuple]:
    pairs = []

    embeddings = np.stack(embeddings)
    embeddings = normalize(embeddings, axis=1)
    distance_matrix = embeddings @ embeddings.T  # type: ignore
    min_idxs = np.argmin(distance_matrix, axis=1, keepdims=True)
    values = np.take_along_axis(distance_matrix, min_idxs, axis=1)

    existed = set()
    for i, value in enumerate(values):
        if value <= sim_th:
            if (i, min_idxs[i][0]) in existed:
                continue
            pairs.append((i, min_idxs[i][0], distance_matrix[i][min_idxs[i][0]]))
            existed.add((min_idxs[i][0], i))

    return pairs
