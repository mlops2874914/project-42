import lakefs  # type: ignore
from lakefs.client import Client  # type: ignore
from pathlib import Path
from loguru import logger  # type: ignore

clt = Client(
    host="http://localhost:8000",
    username="ACCESS_KEY_ID",
    password="SECRET_ACCESS_KEY",
)


def upload_file(repo_name: str, local_filepath: str):
    branch = lakefs.repository(repo_name, client=clt).branch("main")  # type: ignore

    with Path(local_filepath).open("rb") as local_file:
        branch.object(Path(local_filepath).name).upload(local_file.read())

    try:
        commit = branch.commit(
            message="Add some data!", metadata={"using": "python_sdk"}
        ).get_commit()
    except Exception as ex:
        logger.warning(ex)
        commit = branch.get_commit()
        pass

    return commit.id


def read_file(repo_name: str, filename: str, commit_id: str):
    ref = lakefs.repository(repo_name, client=clt).commit(commit_id)  # type: ignore
    with ref.object(filename).reader("rb") as remote_file:
        obj_bytes = remote_file.read()

    return obj_bytes
