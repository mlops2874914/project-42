def bump_version(current_verion: str, limit: int = 10, is_print: bool = False):
    numbers = current_verion.replace('"', "").split(".")
    numbers = [int(num) for num in numbers]  # type: ignore
    if len(numbers) > 1:
        res = 1
        for k in range(len(numbers) - 1, -1, -1):
            numbers[k] = int(numbers[k]) + res  # type: ignore
            if numbers[k] > limit:  # type: ignore
                numbers[k] = 0  # type: ignore
                res = 1
            else:
                res = 0
                break
    else:
        numbers[0] = int(numbers[0]) + 1  # type: ignore

    new_version = ".".join(str(x) for x in numbers)

    if is_print:
        print(f"{new_version}")

    return f'"{new_version}"'
