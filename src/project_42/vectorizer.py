import joblib  # type: ignore
from pathlib import Path

import dvc  # type: ignore
import dvc.api  # type: ignore
import numpy as np
import pandas as pd
import scipy  # type: ignore
import scipy.sparse  # type: ignore
from sklearn.feature_extraction.text import TfidfVectorizer  # type: ignore


def train_vectorizer(src_path: str, dst_dir: str):
    """Train tfidf vectorizer for text

    Args:
        src_path (str): path to source dataset
        dst_dir (str): destination directory
    """
    params = dvc.api.params_show()

    train_split = pd.read_csv(src_path)

    tf_vectorizer = TfidfVectorizer(**params["vectorizer_tfidf"])

    tf_vectorizer.fit(train_split["text_processed"])
    tf_vectorizer._stop_words_id = 0

    joblib.dump(tf_vectorizer, Path(f"{dst_dir}/tf_vectorizer.pickle").open("wb"))


def apply_vectorizer(data_split_path: str, vectorizer_path: str, dst_path: str):
    """Apply tfidf vectorizer to text dataset.
    To archive reproducability features are rounded to 5 decimals

    Args:
        data_split_path (str): path to source dataset
        vectorizer_path (str): trained vectorizer
        dst_path (str): destination directory
    """
    tf_vectorizer = joblib.load(Path(vectorizer_path).open("rb"))
    data_split = pd.read_csv(data_split_path)

    vectorized_split = tf_vectorizer.transform(data_split["text_processed"])
    vectorized_split = round(vectorized_split, 5)

    scipy.sparse.save_npz(f"{dst_path}_vectorized", vectorized_split, compressed=False)

    split_target = data_split["sentiment"]

    np.save(f"{dst_path}_target.npy", split_target)
